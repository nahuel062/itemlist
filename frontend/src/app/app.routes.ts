import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemListComponent } from './components/item-list/item-list.component';
import { AddItemComponent } from './components/add-item/add-item.component';
import { ItemListModule } from './components/item-list/item-list.module';
import { AddItemModule } from './components/add-item/add-item.module';

export const routes: Routes = [
  { path: '', component: ItemListComponent},
  { path: 'add', component: AddItemComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ItemListModule, AddItemModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}