import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routes';
import { AppComponent } from './app.component';
import { ItemListModule } from './components/item-list/item-list.module';
import { AddItemModule } from './components/add-item/add-item.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ItemListModule, AddItemModule, HttpClientModule],
  bootstrap: [AppComponent],
})
export class AppModule {}