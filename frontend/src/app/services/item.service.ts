import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  private apiUrl = 'http://localhost:5000/Item/items';

  constructor() {}

  getItemList(): Observable<any[]> {
    return new Observable<any[]>((observer) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', this.apiUrl, true);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            const response = JSON.parse(xhr.responseText);
            observer.next(response);
            observer.complete();
          } else {
            observer.error(`Error ${xhr.status}: ${xhr.statusText}`);
          }
        }
      };
      xhr.send();
    });
  }

  addItem(newItem: any): Observable<any> {
    return new Observable<any>((observer) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', this.apiUrl, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 201) {
            const response = JSON.parse(xhr.responseText);
            observer.next(response);
            observer.complete();
          } else {
            observer.error(`Error ${xhr.status}: ${xhr.statusText}`);
          }
        }
      };
      xhr.send(JSON.stringify(newItem));
    });
  }

}