import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../services/item.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit {
  itemList: { name: string }[] = [];

  constructor(private itemService: ItemService) {}

  ngOnInit(): void {
    this.loadItemList();
  }

  private loadItemList(): void {
    this.itemService.getItemList().subscribe(
      (items: any[]) => {
        this.itemList = items;
      },
      (error) => {
        console.error('Error fetching items:', error);
      }
    );
  }
}