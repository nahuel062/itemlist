import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AddItemComponent } from './add-item.component';

@NgModule({
  declarations: [AddItemComponent],
  imports: [CommonModule, FormsModule],
})
export class AddItemModule {}