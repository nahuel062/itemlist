import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../services/item.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
})
export class AddItemComponent implements OnInit {
  newItemName: string = '';

  constructor(private itemService: ItemService) { }

  addItem() {
    if (this.newItemName.trim() !== '') {
      const newItem = { name: this.newItemName };
      this.itemService.addItem(newItem).subscribe(
        (response) => {
          console.log('Item agregado correctamente:', response);
          this.newItemName = '';
        },
        (error) => {
          console.error('Error al agregar el item:', error);
        }
      );
    }
  }

  ngOnInit(): void {
  }
}