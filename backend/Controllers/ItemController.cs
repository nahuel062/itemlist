﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ItemController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet("items")]
        public async Task<ActionResult<IEnumerable<Item>>> GetItems()
        {
            var items = await _context.Items.ToListAsync();
            return Ok(items);
        }

        [HttpGet("items/{id}")]
        public async Task<ActionResult<Item>> GetItemById(int id)
        {
            var item = await _context.Items.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPost("items")]
        public async Task<ActionResult<Item>> CreateItem([FromBody] Item newItem)
        {
            if (newItem == null)
            {
                return BadRequest();
            }

            _context.Items.Add(newItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetItemById), new { id = newItem.Id }, newItem);
        }

        [HttpPut("items/{id}")]
        public async Task<ActionResult<Item>> UpdateItem(int id, [FromBody] Item updatedItem)
        {
            var existingItem = await _context.Items.FindAsync(id);

            if (existingItem == null)
            {
                return NotFound();
            }

            existingItem.Name = updatedItem.Name;

            _context.Entry(existingItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(existingItem);
        }

        [HttpDelete("items/{id}")]
        public async Task<ActionResult<Item>> DeleteItem(int id)
        {
            var itemToDelete = await _context.Items.FindAsync(id);

            if (itemToDelete == null)
            {
                return NotFound();
            }

            _context.Items.Remove(itemToDelete);
            await _context.SaveChangesAsync();

            return Ok(itemToDelete);
        }
    }
}
