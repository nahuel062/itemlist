using System.ComponentModel.DataAnnotations.Schema;

[Table("Items")]
public class Item
{
    public int Id { get; set; }
    public string Name { get; set; }
}